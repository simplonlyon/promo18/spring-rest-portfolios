# Spring Portfolios
API Rest en Spring Boot permettant d'interagir avec une liste de [Student](src/main/java/co/simplon/p18/portfolios/entity/Student.java) possédant une liste de [Projects](src/main/java/co/simplon/p18/portfolios/entity/Project.java)

[Lien vers le projet front](https://gitlab.com/simplonlyon/promo18/angular-portfolio)

## Exercices
### Contrôleur Spring boot ([fichier](src/main/java/co/simplon/p18/portfolios/controller/StudentController.java))
1. Cloner le projet et créer une base de données correspondante à ce qu'il y a dans le application.properties
2. Lancer les tests des repositories pour vérifier que ça marche
3. Créer le contrôleur StudentController et dedans commencer par une méthode GET sur /api/student qui va renvoyer la liste des student
4. Vérifier que ça fonctionne, en lançant le test testGetAll() situé dans le StudentControllerTest
5. Une fois que ça marche, on peut passer sur un GET pour /api/student/id, et vérifier que ça marche en lançant le test testGetById (petite particularité, pour cette route on souhaite que quand on récupère un student, on récupère également ses projects)
6. Sur cette même route GET /api/student/id, il faut aussi que le testGetByIdNotFound passe 

### Appli angular et affichage des students ([fichiers](https://gitlab.com/simplonlyon/promo18/angular-portfolio/-/tree/main/src/app))
1. Créer une appli angular-portfolio avec le routing
2. Dans le index.html, mettre le link de bootstrap
3. Dans le src/app créer un fichier entities.ts et dedans créer des interfaces pour Student et pour Project, en faisant en sorte que l'id soit optionnel pour les deux
4. Générer un component HomeComponent et lui créer une route dans le AppRoutingModule pour faire que ça soit la page d'accueil par défaut
5. Générer un StudentService et dedans mettre les deux méthodes pour le getAll et pour le getOne (donc ajouter le HttpClientModule, injecter le HttpClient dans  le service etc.)
6. Dans le HomeComponent, injecter le StudentService et l'utiliser pour aller chercher la liste des students qu'on assigne à une propriété
7. Générer un component StudentComponent qui va avoir en propriété un Student avec un @Input pour faire qu'il vienne de l'extérieur
8. Dans le template home.component.html, faire une boucle qui va afficher un app-student à chaque tour
9. Faire le templating pour que ça affiche les student sous forme de cards boostrap par exemple 

### Afficher une page pour un Student ([fichiers](https://gitlab.com/simplonlyon/promo18/angular-portfolio/-/tree/main/src/app/single-student))
1. Générer un component SingleStudent
2. Créer une route pour le SingleStudentComponent dans le AppRoutingModule, et faire que cette route attende un paramètre, nous on veut que la route ça soit `student/:id` où id sera le paramètre dynamique
3. Dans le SingleStudentComponent, injecter dans le constructor un paramètre de type ActivatedRoute
4. Dans le ngOnInit utilisé le ActivatedRoute faire un subscribe sur la propriété params et dans ce subscribe, récupérer l'id et en faire un console.log pour le moment
5. Dans le student-card.component.html, mettre un [routerLink] sur le lien See projects et faire qu'il pointe vers '/student/' concaténé avec l'id du student
6. Une fois que vous arrivez à récupérer l'id, injecter le StudentService dans le constructor, et déclarer une propriété student?:Student
7. À la place de votre console.log déclencher un getById et se subscribe dessus et assigner le data au student (c'est pas la meilleure manière de faire, mais on regardera une façon mieux après)

### Afficher les student d'une session donnée
#### Côté Back ([fichier](src/main/java/co/simplon/p18/portfolios/controller/StudentController.java))
1. Modifier la route getAll du StudentController en lui rajoutant un RequestParam optionnel dans ses arguments, qu'on appellera `session` et qui sera du String
2. Dans la méthode, vérifier si l'argument session est null, si oui, on fait le findAll comme avant, si non, on fait un findBySession
3. Vous pouvez tester si ça marche en relançant le testGetAll et le testGetByPromo

#### Côté Front ([fichiers](https://gitlab.com/simplonlyon/promo18/angular-portfolio/-/tree/main/src/app/session))
1. Créer une nouvelle méthode dans le StudentService qui va attendre une string session en argument et qui fera une requête vers `/api/student?session=...` et renverra un Observable<Student[]>
2. Générer un nouveau component SessionComponent et le lier à une route paramétrée `session/:session`
3. Dans ce session component, faire comme on a fait dans le SingleStudentComponent, en injectant le ActivatedRoute et le StudentService puis dans le ngOnInit récupérer le param session et s'en servir pour lancer la méthode qu'on vient de faire dans le service
4. Dans le template faire une boucle sur les students, exactement de la même manière que dans le home.component.html
5. Modifier le student-card.component.html pour faire que la session du student soit un lien avec un routerLink pointant sur la route session