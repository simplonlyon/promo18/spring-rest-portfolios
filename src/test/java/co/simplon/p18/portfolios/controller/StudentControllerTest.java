package co.simplon.p18.portfolios.controller;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.List;

import javax.print.attribute.standard.Media;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;

import co.simplon.p18.portfolios.entity.Project;
import co.simplon.p18.portfolios.entity.Student;
import co.simplon.p18.portfolios.repository.ProjectRepository;
import co.simplon.p18.portfolios.repository.StudentRepository;

@WebMvcTest(controllers = StudentController.class)
public class StudentControllerTest {

    @Autowired
    MockMvc mvc;

    @MockBean
    ProjectRepository proRepo;
    @MockBean
    StudentRepository stuRepo;

    List<Student> testData = List.of(
            new Student(1, "test1", "test1@test.com", "session"),
            new Student(2, "test2", "test2@test.com", "session"),
            new Student(3, "test3", "test3@test.com", "session2"));

    @Test
    public void testGetAll() throws Exception {
        when(stuRepo.findAll()).thenReturn(testData);
        mvc.perform(get("/api/student"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$[0].name").value("test1"));

        verify(stuRepo).findAll();
    }

    @Test
    public void testGetByPromo() throws Exception {
        when(stuRepo.findBySession("test")).thenReturn(testData);
        mvc.perform(get("/api/student?session=test"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$[0].name").value("test1"));

        verify(stuRepo).findBySession("test");

    }

    @Test
    public void testGetById() throws Exception {
        when(stuRepo.findById(1)).thenReturn(testData.get(0));
        when(proRepo.findByIdStudent(1)).thenReturn(List.of(
                new Project(1, "testpro1", "techs", "picture"),
                new Project(2, "testpro2", "techs", "picture")));

                
        mvc.perform(get("/api/student/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("test1"))
                .andExpect(jsonPath("$.projects").isArray())
                .andExpect(jsonPath("$.projects[1].name").value("testpro2"));

        verify(stuRepo).findById(1);
        verify(proRepo).findByIdStudent(1);

    }

    @Test
    public void testGetByIdNotFound() throws Exception {
        when(stuRepo.findById(10)).thenReturn(null);

        mvc.perform(get("/api/student/10"))
                .andExpect(status().isNotFound());

        verify(stuRepo).findById(10);

    }

    @Test
    public void testAdd() throws Exception {

        mvc.perform(
                post("/api/student")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("""
                                {
                                    "name":"name",
                                    "mail":"mail@mail.com",
                                    "session":"session"
                                }
                                """))
                .andExpect(status().isCreated());

    }

    @Test
    public void testAddNotValid() throws Exception {

        mvc.perform(post("/api/student")
                .contentType(MediaType.APPLICATION_JSON)
                .content("""
                        {
                            "name": "",
                            "mail":"mail",
                            "session":"session"
                        }
                        """))
                .andExpect(status().isBadRequest());

    }

    @Test
    public void testUpdate() throws Exception {
        when(stuRepo.findById(1)).thenReturn(testData.get(0));

        mvc.perform(
                put("/api/student/1")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("""
                                {
                                    "id":1,
                                    "name":"name",
                                    "mail":"mail@mail.com",
                                    "session":"session"
                                }
                                """))
                .andExpect(status().isOk());
        verify(stuRepo).findById(1);

    }

    @Test
    public void testUpdateNotFound() throws Exception {
        when(stuRepo.findById(10)).thenReturn(null);

        mvc.perform(
                put("/api/student/10")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content("""
                                {
                                    "id":10,
                                    "name":"name",
                                    "mail":"mail@mail.com",
                                    "session":"session"
                                }
                                """))
                .andExpect(status().isNotFound());

        verify(stuRepo).findById(10);
    }

    @Test
    public void testDelete() throws Exception {
        when(stuRepo.findById(1)).thenReturn(testData.get(0));
        
        mvc.perform(delete("/api/student/1"))
                .andExpect(status().isNoContent());

        verify(stuRepo).findById(1);

    }

    @Test
    public void testDeleteNotFound() throws Exception {
        when(stuRepo.findById(10)).thenReturn(null);
        
        mvc.perform(delete("/api/student/10"))
                .andExpect(status().isNotFound());

        verify(stuRepo).findById(10);

    }

}
