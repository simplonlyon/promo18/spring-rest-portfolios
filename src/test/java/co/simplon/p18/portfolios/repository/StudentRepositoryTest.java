package co.simplon.p18.portfolios.repository;

import static org.assertj.core.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;

import co.simplon.p18.portfolios.entity.Student;

@SpringBootTest
@Sql("/database.sql")
public class StudentRepositoryTest {

    @Autowired
    StudentRepository repo;

    @Test
    void testDelete() {
        assertThat(repo.delete(1)).isTrue();

    }

    @Test
    void testDeleteNotExists() {
        assertThat(repo.delete(100)).isFalse();

    }

    @Test
    void testFindAll() {
        assertThat(repo.findAll())
                .hasSize(4)
                .doesNotContainNull()
                .allSatisfy(student -> assertThat(student).hasNoNullFieldsOrProperties());
    }

    @Test
    void testFindById() {
        Student student = repo.findById(1);
        assertThat(student)
                .hasNoNullFieldsOrProperties();
        
        
    }
    @Test
    void testFindByIdNotExists() {
        assertThat(repo.findById(100))
                .isNull();
    }

    @Test
    void testFindBySession() {
        assertThat(repo.findBySession("promo 1"))
                .hasSize(2)
                .doesNotContainNull()
                .allSatisfy(student -> assertThat(student).hasNoNullFieldsOrProperties());

    }

    @Test
    void testSave() {
        Student student = new Student("nom", "mail@mail.com", "promo test");

        repo.save(student);

        assertThat(student.getId()).isNotNull();
    }

    @Test
    void testUpdate() {
        Student student = new Student(1, "nom", "mail@mail.com", "promo test");

        assertThat(repo.update(student)).isTrue();
    }

    @Test
    void testUpdateNotExists() {
        Student student = new Student(100, "nom", "mail@mail.com", "promo test");

        assertThat(repo.update(student)).isFalse();
    }
}
