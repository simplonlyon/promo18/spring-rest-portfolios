package co.simplon.p18.portfolios;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PortfoliosApplication {

	public static void main(String[] args) {
		SpringApplication.run(PortfoliosApplication.class, args);
	}

}
