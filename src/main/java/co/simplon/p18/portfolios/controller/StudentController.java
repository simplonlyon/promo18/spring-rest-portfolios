package co.simplon.p18.portfolios.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.p18.portfolios.entity.Student;
import co.simplon.p18.portfolios.repository.ProjectRepository;
import co.simplon.p18.portfolios.repository.StudentRepository;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;


@RestController
@RequestMapping("/api/student")
public class StudentController {

    @Autowired
    private StudentRepository stuRepo;
    @Autowired
    private ProjectRepository proRepo;

    /**
     * La route getAll a un RequestParam optionnel, cela signifie que l'on pourra y
     * accéder aussi bien sur cette url http://localhost:8080/api/student
     * que sur celle ci http://localhost:8080/api/student?session=valeur
     * Dans le premier cas, on fera un findAll pour récupérer tous les student,
     * dans le deuxième, on fera un findBySession pour récupérer uniquement les students
     * de la session indiqué en paramètre
     * @param session Paramètre optionnel ?session=valeur pour indiquer la promo dont on veut les students
     * @return Une liste de student
     */
    @GetMapping
    public List<Student> getAll(@RequestParam(required = false) String session) { //Alternativement on peut faire avec un Optional<String>, dans le cas présent ça revient au même
        if(session != null) {
            return stuRepo.findBySession(session);
        }
        return stuRepo.findAll();
    }

    @GetMapping("/{id}")
    public Student getOne(@PathVariable int id) {
        Student student = stuRepo.findById(id);
        if(student == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        student.setProjects(proRepo.findByIdStudent(id));
        return student;
    }
    

    
    
    
}
